FROM golang:alpine as builder

WORKDIR /app
COPY . .
RUN go mod download && go build -o /lab2

FROM alpine
COPY --from=builder /lab2 /lab2
EXPOSE 8000
CMD [ "/lab2" ]