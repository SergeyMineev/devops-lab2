FROM golang:alpine

WORKDIR /app

COPY . .
RUN go mod download && go build -o /lab2

EXPOSE 8000
CMD [ "/lab2" ]